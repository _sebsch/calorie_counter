from flask import Flask
from flask_cors import CORS

from models import db

app = Flask(__name__)
app.config.from_pyfile('./app.cfg')
CORS(app)
db.init_app(app)

with app.app_context():
    db.create_all()

from views.api import rest_api

app.register_blueprint(rest_api, url_prefix="/api/v1/")
