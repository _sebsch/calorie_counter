from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
from datetime import datetime


class Meal(db.Model):
    __tablename__ = 'meals'

    id = db.Column(db.Integer, primary_key=True)

    description = db.Column(db.Text)
    gram = db.Column(db.Integer)
    calories = db.Column(db.Float)
    timestamp = db.Column(db.DateTime)

    def __init__(self, description, gram, calories, timestamp):
        from app import app

        self.date_format = app.config["DATE_FORMAT"]

        self.description = description
        self.gram = gram
        self.calories = calories
        self.timestamp = datetime.strptime(timestamp, self.date_format)

    def __repr__(self):
        return f"" \
            f"<Meal " \
            f"{self.description!r} " \
            f"{self.timestamp.strftime(self.date_format)!r}" \
            f">"
