from flask import Blueprint, render_template

front_page = Blueprint(
    "front_page",
    __name__,
    template_folder="templates"
)


@front_page.route("/")
def index():
    return render_template('index.html', mimetype='text/html')
