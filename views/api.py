from flask import Blueprint, jsonify, make_response
from flask import request
from datetime import datetime

from app import app
from app import db

from models import Meal

import json

rest_api = Blueprint(
    "rest_api",
    __name__,
)

DATE_API_DEFAULTS = {
    "date": datetime.now().strftime(app.config['DATE_FORMAT'])
}

"""
{
    'today': [
        {
            'description': "something", 
            'gram': 42, 
            'calories': 23,
            'timestamp': "today"
        },
        (...)
    'sum': {'calories': "Σ_cal"}
    ]
}
"""


@rest_api.route("/", defaults=DATE_API_DEFAULTS, methods=['GET'])
@rest_api.route("/<date>", methods=['GET'])
def get_data_from_date(date):
    meals_from_date = db.session.query(Meal) \
        .filter(Meal.timestamp == datetime.strptime(date, app.config['DATE_FORMAT'])) \
        .all()

    meals_answer = {
        'results': [
            {'description': meal.description,
             'gram': meal.gram,
             'calories': meal.calories,
             'timestamp': meal.timestamp
             }
            for meal in meals_from_date],
        'sum':
            sum([m.calories for m in meals_from_date])
    }

    resp = make_response(meals_answer)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@rest_api.route("/", methods=['POST'])
def post_data_from_date():
    request_json = request.get_json()

    if 'timestamp' not in request_json:
        request_json['timestamp'] = datetime.now().strftime(app.config['DATE_FORMAT'])

    meal = Meal(**request_json)
    db.session.add(meal)
    db.session.commit()

    return jsonify({'added': f"{meal}"})
