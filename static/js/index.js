var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!'
    }
});

var app2 = new Vue({
    el: '#app-2',
    data: {
        message: 'You loaded this page on ' + new Date().toLocaleString()
    }
})

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

const vm = new Vue({
    el: '#api_client',
    data: {
        results: []
    },
    mounted() {
        axios.get("http://localhost:5000/api/v1/", {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then(response => {
                    this.results = response.data.results
                },
            ).catch(function (error) {
            if (error.response) {
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log(error.message);
            }
            console.log(error.config);
        })
    }
})